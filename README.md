# Accurate Player Media Tools

This repository contains free and open source scripts that may be useful when working with media. Use the tools in this
repository at your own risk, see them more are useful examples on how to acomplish something instead of using it
straight off.

Many of the tools use other open source software as [FFmpeg](https://ffmpeg.org/) and [ImageMagick](https://imagemagick.org/) so you might need to install those before using the scripts provided here.

## Table of contents

| Path         | Description                    |
|--------------|--------------------------------|
| `/thumbnail` | Tools for thumbnail generation |
