#!/usr/bin/env python3

W = 240
H = -1

import argparse
import json
import math
import os
import subprocess
import sys
import uuid
from decimal import Decimal, ROUND_HALF_UP
from itertools import count


def roundup(num):
    return int(Decimal(num).quantize(Decimal(0), rounding=ROUND_HALF_UP))


def extract_stills(input_path, output_path, fps_numerator, fps_denominator, frame_interval):
    # mask this error https://trac.ffmpeg.org/ticket/4827
    p1 = "End mismatch 1".encode("utf8")
    p2 = "Last message repeated".encode("utf8")
    p3 = "Output file is empty".encode("utf8")
    for i in range(1, 1000000):
        output = os.path.join(output_path, "still%d.png" % i)
        if os.path.exists(output):
            os.remove(output)
        # frame_interval is always even, no rounding necessary
        ps = subprocess.Popen(["ffmpeg", "-v", "warning", "-accurate_seek",
                               "-ss", "{:.9f}".format((fps_denominator * frame_interval * (i - 0.5)) / fps_numerator + 0.000000999), "-i", input_path,
                               "-vf", "scale=%d:%d" % (W, H), "-qscale:v", "3", "-frames:v", "1", "-fps_mode", "vfr", output], stderr=subprocess.PIPE)
        try:
            for line in ps.stderr:
                if not (p1 in line or p2 in line or p3 in line):
                    sys.stderr.write(line.decode("utf8"))
                elif i == 1 and p3 in line:
                    sys.stderr.write("Failed to extract first sprite\n")
                    sys.exit(1)
            r = ps.wait()
            if r:
                sys.stderr.write("ffmpeg failed with exit code %d\n" % r)
                sys.exit(1)
            elif not os.path.isfile(output):
                if i == 1:
                    sys.stderr.write("Failed to extract first sprite\n")
                    sys.exit(1)
                break
        finally:
            ps.kill()


def collect_images(base_path):
    paths = []
    for i in count(1):
        f = os.path.join(base_path, "still%d.png" % i)
        if not os.path.isfile(f):
            break
        paths.append(f)
    return paths


def get_rows_and_columns(paths):
    rows = int(math.ceil(math.sqrt(len(paths))))
    columns = int(math.ceil(len(paths) / rows))
    return rows, columns


def build_sprite_map(paths, output_spritemap):
    rows, columns = get_rows_and_columns(paths)
    column_paths = []
    for i in range(rows):
        output_spritemap_column = "%s_%s_%s" % (i, uuid.uuid4(), output_spritemap)
        start = i * columns
        end = start + columns
        r = subprocess.call(
            ["montage", "-background", "black", "-tile", "%dx%d" % (columns, 1), "-geometry", "+0+0"] +
            paths[start:end] + [output_spritemap_column])
        column_paths.append(output_spritemap_column)
        if r != 0:
            break
    else:
        r = subprocess.call(
            ["montage", "-background", "black", "-tile", "%dx%d" % (1, rows), "-geometry", "+0+0"] + column_paths + [output_spritemap])
    for path in paths:
        os.remove(path)
    for path in column_paths:
        os.remove(path)
    if r != 0:
        sys.exit(r)


def get_height(paths):
    r = subprocess.check_output(['convert', paths[0], '-print', '%h', '/dev/null'])
    return int(r)


def build_manifest(paths, fps_numerator, fps_denominator, frame_interval):
    sprites = []
    rows, columns = get_rows_and_columns(paths)

    for i in range(0, len(paths)):
        x = W * (i % columns)
        y = H * int(i / columns)
        frame = frame_interval * (i + 0.5)  # frame_interval is always even, no rounding necessary
        flicks = round((705600000.0 * frame * fps_denominator) / fps_numerator)
        sprites.append({
            "x": x,
            "y": y,
            "t": flicks,
        })
    return {
        "width": W,
        "height": H,
        "sprites": sprites
    }


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input', metavar='VIDEO_FILE', nargs=1)
    parser.add_argument('-n', dest='fps_numerator', type=int, required=True, help='FPS numerator')
    parser.add_argument('-d', dest='fps_denominator', type=int, required=True, help='FPS denominator')
    parser.add_argument('-W', dest='image_width', type=int, required=True, help='Image width')
    parser.add_argument('-H', dest='image_height', type=int, required=False, help='Image height')
    parser.add_argument('-i', dest='interval', type=float, required=True, help='Interval (in frames)')
    parser.add_argument('-om', dest='output_manifest', default="manifest.json", type=str, required=False, help='Output manifest file')
    parser.add_argument('-os', dest='output_spritemap', default="spritemap.jpg", type=str, required=False,
                        help='Output spritemap file')
    args = parser.parse_args()
    W = args.image_width
    interval = max(2 * math.ceil(args.interval / 2), 2)  # interval must be even
    extract_stills(args.input[0], "./", args.fps_numerator, args.fps_denominator, interval)
    paths = collect_images("./")
    H = get_height(paths)
    build_sprite_map(paths, args.output_spritemap)
    sprites = build_manifest(paths, args.fps_numerator, args.fps_denominator, interval)
    json.dump(sprites, open(args.output_manifest, "w"))

