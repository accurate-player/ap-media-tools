# Thumbnail tools

Tools for generating thumbnails from video files.

## spritemap.py

This tool uses [FFmpeg](https://ffmpeg.org/) and [ImageMagick](https://imagemagick.org/) to create a single sprite map
image with thumbnails from a video.

### Usage

```shell
usage: spritemap.py [-h] -n FPS_NUMERATOR -d FPS_DENOMINATOR -W IMAGE_WIDTH [-H IMAGE_HEIGHT] -i INTERVAL [-om OUTPUT_MANIFEST] [-os OUTPUT_SPRITEMAP] VIDEO_FILE

positional arguments:
  VIDEO_FILE

options:
  -h, --help            show this help message and exit
  -n FPS_NUMERATOR      FPS numerator
  -d FPS_DENOMINATOR    FPS denominator
  -W IMAGE_WIDTH        Image width
  -H IMAGE_HEIGHT       Image height
  -i INTERVAL           Interval (in frames)
  -om OUTPUT_MANIFEST   Output manifest file
  -os OUTPUT_SPRITEMAP  Output spritemap file
```

### Example

```shell
./spritemap.py BigBuckBunny.mp4 -n 25 -d 1 -W 240 -i 500 -om manifest.json -os spritemap.jpg
```

This creates a sprite map `spritemap.jpg` and a manifest file `manifest.json` from the input file `BigBuckBunny.mp4` (25
fps)
